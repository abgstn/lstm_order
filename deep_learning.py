import numpy as np
import tensorflow as tf
from generate_data import DataGenerator, Preprocess
import pandas as pd


class seq2seq:
    def __init__(self, seq_len=None, feature_dim=None,
                 input_data=None, print_every=1, layer_hidden_size=None):
        self.seq_len = seq_len
        self.feature_dim = feature_dim
        self.input_data = input_data
        self.print_every = print_every
        self.layers_size = layer_hidden_size if layer_hidden_size is not None else [512, 512]

        # build model
        self.graph = self.build_model()
        self.sess = tf.Session(graph=self.graph)

    def build_model(self):
        # basic info
        seq_len, feature_num = self.seq_len, self.feature_dim

        # initiate graph
        g = tf.Graph()

        with g.as_default():
            # input placeholder
            input_x = tf.placeholder(tf.float32, [None, seq_len, feature_num])
            input_y = tf.placeholder(tf.float32, [None, seq_len, 1])

            # model
            dropout = tf.nn.dropout(input_x, keep_prob=0.3)
            lstm_cells = [tf.nn.rnn_cell.LSTMCell(l) for l in self.layers_size]
            cell = tf.nn.rnn_cell.MultiRNNCell(lstm_cells)
            hidden, state = tf.nn.dynamic_rnn(cell, dropout, dtype=tf.float32)
            outputs = tf.layers.dense(hidden, 1)

            # loss
            loss_summary = tf.losses.mean_squared_error(outputs, input_y)

            # store node
            for node in (input_x, input_y, outputs, loss_summary):
                g.add_to_collection("important node", node)
        return g

    def optimize(self):
        input_x, input_y, _, loss_summary = self.graph.get_collection('important node')
        with self.graph.as_default() as g:
            train_step = tf.train.AdagradOptimizer(0.3).minimize(loss_summary)
            g.add_to_collection("optimizer", train_step)

    def train(self):
        self.optimize()
        graph = self.graph
        input_x, input_y, outputs, loss_summary = graph.get_collection("important node")
        train_step = graph.get_collection("optimizer")

        sess = self.sess
        validset = self.input_data['valid']
        with graph.as_default(), sess.as_default():
            tf.global_variables_initializer().run()

            for i, batch in enumerate(self.input_data['train']):
                x, y = batch
                loss, _ = sess.run([loss_summary, train_step],
                                   feed_dict={input_x: x, input_y: y})

                if self.print_every is not None and i % self.print_every == 0:
                    valid_loss = loss_summary.eval({input_x: validset[0], input_y: validset[1]})
                    print("Step:", i, "Loss:", loss, "valid loss:", valid_loss)

    def predict(self, input_data):
        sess = self.sess
        input_x, _, outputs, _ = sess.graph.get_collection("important node")
        x, _ = input_data
        with self.graph.as_default(), self.sess.as_default():
            prediction = sess.run(outputs, feed_dict={input_x: x})
        return prediction


if __name__ == "__main__":
    batch_size = 32
    lstm_size = [512, 512]
    feature_num = 45
    num_rows = {'train': 1000, 'valid': 50, 'test': 50}
    seq_len = 10

    # data = DataGenerator(num_rows=num_rows,
    #                      feature_num=feature_num,
    #                      batch_size=batch_size,
    #                      seq_len=seq_len).generate()
    cols = ['sub_geo', 'group_id', 'category']
    target = ['quantity']
    cat_vars = ['sub_geo', 'group_id', 'category', 'business_type']

    df = Preprocess.read_csv('data/clean_order_test.csv')

    df = df[['family_desc', 'group_id', 'sub_geo', 'business_type', 'category', 'quantity', 'date']]
    df.date = pd.to_datetime(df.date).dt.strftime('%Y-%m-%d')
    df = Preprocess.impute_week(df)

    dg = Preprocess(data=df, features=cols, target=target,
                    cat_vars=cat_vars, split_on=['week_index'])
    train, test = dg.run()

    model = seq2seq(seq_len=seq_len,
                    feature_dim=feature_num,
                    layer_hidden_size=lstm_size,
                    input_data=df)
    model.train()

