from keras.layers import Input, Dense, Dropout
from keras.models import Model
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.optimizers import Adadelta
import numpy as np


# df = pd.read_csv('data/family_config_tfidf.csv', keep_default_na=False, low_memory=False)
x_train = np.load('data/tfidf_mat_2grams.npy', encoding='latin1').item()
x_train = x_train * 500
rate = 0.3
# encoding_dim = 2

tfidf_input = Input(shape=(x_train.shape[1],))
dropout = Dropout(rate)(tfidf_input)
encoded = Dense(32, activation='relu')(dropout)
encoded = Dense(10, activation='relu')(encoded)

decoded = Dense(32, activation='relu')(encoded)
decoded = Dense(x_train.shape[1], activation='relu')(decoded)

checkpoint = ModelCheckpoint('model/deep_autoencoder/model{epoch:d}-{val_loss:.4f}.h5',
                             period=10, save_best_only=False)
es = EarlyStopping(monitor='val_loss', min_delta=0.0005, patience=10, verbose=0, mode='auto',
                   baseline=None, restore_best_weights=True)
autoencoder = Model(tfidf_input, decoded)
encoder = Model(tfidf_input, encoded)
# optim = Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0)
autoencoder.compile(optimizer='adadelta', loss='mean_squared_error')
autoencoder.fit(x_train, x_train, epochs=10000, verbose=1, validation_split=0.3,
                batch_size=32, shuffle=True, callbacks=[checkpoint, es])
autoencoder.save("model/deep_autoencoder/model_best_through.h5")

encoder_out = encoder.predict(x_train)

# print(encoder_out)