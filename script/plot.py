import re
from collections import defaultdict
import pandas as pd
import matplotlib.pyplot as plt

result = defaultdict(list)
with open('../data/file.txt', 'r') as f:
    for line in f:
        if '-' in line:
            tmp = [re.sub("[^0-9\.]", '', re.sub('\.h5', '', item)) for item in line.split('-')]
            result['iter'].append(tmp[0])
            result['valloss'].append(tmp[1])

result = pd.DataFrame.from_dict(result)
result.iter = pd.to_numeric(result.iter)
result.valloss = pd.to_numeric(result.valloss)
result = result.sort_values('iter')
result.plot('iter', 'valloss', kind='line')
plt.show()

