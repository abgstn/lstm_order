import numpy as np
import pandas as pd
from sklearn import preprocessing as skl_preprocess


class DataGenerator:
    def __init__(self, num_rows=None, feature_num=45, batch_size=32, seq_len=10):
        self.num_rows = dict(zip(['train', 'valid', 'test'], [10000, 32, 32])) if \
            num_rows is None else num_rows
        self.feature_num = feature_num
        self.batch_size = batch_size
        self.seq_len = seq_len

    def generate_data(self):
        data = {}
        for k, v in self.num_rows.items():
            train_y = np.random.uniform(0, 1, [v, self.seq_len, 1])
            train_x = np.random.uniform(0, 1, [v, self.seq_len, self.feature_num])
            train = (train_x, train_y)
            if k == 'train':
                train = DataGenerator.generate_batch(train,
                                                     self.num_rows['train'],
                                                     self.batch_size)
            data[k] = train
        return data

    @staticmethod
    def generate_batch_idx(tot, batch_size):
        idx = np.array([i // batch_size for i in range(tot)])
        np.random.shuffle(idx)
        return idx

    @staticmethod
    def generate_batch(data, tot, batch_size):
        idx = DataGenerator.generate_batch_idx(tot, batch_size)
        train_x, train_y = data
        for i in set(idx):
            batch_x = train_x[idx == i]
            batch_y = train_y[idx == i]
            yield batch_x, batch_y


class Preprocess:
    def __init__(self, data=None, batch_size=128, features=None, stratify_vars=None,
                 target=None, cat_vars=None, split_on=None):
        self.batch_size = batch_size
        self.features = features
        self.data = data
        self.stratify_vars = stratify_vars if stratify_vars is not None else ['family_desc', 'sub_geo', 'group_id']
        self.target = target
        self.categorical_vars = cat_vars
        self.le = None
        self.split_on = split_on if split_on is not None else ['week_index']

    @staticmethod
    def read_csv(file_path):
        data = pd.read_csv(file_path, low_memory=False, keep_default_na=False)
        return data

    @staticmethod
    def split_on_idx(idx, id2keep, data):
        indices = [True if id in id2keep else False for id in idx]
        return data.loc[indices, :].reset_index(drop=True)

    def get_xy(self, split_on, data):
        self.get_le(data)
        features = list(set(self.features + split_on))
        transformed_df = self.get_transformed_df(data).loc[:, features + self.target]
        return Preprocess.convert_to_array((features, self.target), split_on,
                                           transformed_df)

    def get_transformed_df(self, df):
        tmp_df = df.loc[:, self.categorical_vars].apply(lambda x: self.le[x._name].transform(x))
        col_idx = [col for col in df.columns if col not in self.categorical_vars]
        tmp_df = pd.concat([tmp_df, df.loc[:, col_idx]], axis=1)
        return tmp_df

    def get_le(self, df):
        le_dict = {}
        for col in df.columns:
            if col in self.categorical_vars:
                le = skl_preprocess.LabelEncoder()
                le_dict[col] = le.fit(df.loc[:, col])
        self.le = le_dict

    @staticmethod
    def convert_to_array(vars_list, split_on, data):
        tmp_df = data.copy(deep=True)
        max_seq_len = tmp_df.loc[:, split_on[0]].max()
        res = []
        y = []
        for w_idx in range(0, max_seq_len):
            # convert x to matrix
            m = tmp_df.loc[tmp_df.loc[:, split_on[0]] == w_idx, vars_list[0]].values
            m = m.reshape(m.shape[0], -1, m.shape[1])
            res.append(m)

            # convert y to matrix
            m = tmp_df.loc[tmp_df.loc[:, split_on[0]] == w_idx, vars_list[1]].values
            m = m.reshape(m.shape[0], -1, m.shape[1])
            y.append(m)

        res = np.concatenate(res, axis=1)
        y = np.concatenate(y, axis=1)
        return res, y

    def train_test_split(self, idx_col):
        data = self.data
        # group_idx = data.loc[:, idx_col].apply(lambda x: '@'.join([str(i) for i in x.values]), axis=1)
        # idx = group_idx.unique()
        #
        # train_idx = np.random.choice(idx, size=int(len(idx) * 0.8), replace=False)
        # test_idx = set(idx) - set(train_idx)
        # train_data = self.split_on_idx(group_idx, train_idx, data)
        # test_data = self.split_on_idx(group_idx, test_idx, data)

        grouped_df = data.groupby(idx_col)
        group_idx = grouped_df.ngroup()
        sample_idx = np.arange(grouped_df.ngroups)
        np.random.shuffle(sample_idx)

        to = int(len(sample_idx)*0.8)
        train_data = data.loc[group_idx.isin(sample_idx[:to]), :]
        test_data = data.loc[group_idx.isin(sample_idx[to:]), :]

        return train_data, test_data

    def generate_batch(self, data, idx_col):
        # group_idx = data.loc[:, idx_col].apply(lambda x: '@'.join([str(i) for i in x.values]), axis=1)
        # idx = group_idx.unique()
        #
        # ids = np.array([i // len(idx) for i in range(len(idx))])
        # np.random.shuffle(ids)
        # # result = []
        # for i in set(ids):
        #     keys = idx[ids == i]
        #     batch = self.split_on_idx(group_idx, keys, data)
        #     batch_x, batch_y = self.get_xy(self.split_on, batch)
        #     yield (batch_x, batch_y)
        # return result
        self.get_le(data)
        features = list(set(self.features + self.split_on))
        transformed_df = self.get_transformed_df(data)
        X = transformed_df.groupby(idx_col). \
            apply(lambda x: x.sort_values(self.split_on).loc[:, features].values) \
            .tolist()

        y = transformed_df.groupby(idx_col). \
            apply(lambda x: x.sort_values(self.split_on).loc[:, self.target].values) \
            .tolist()

        return np.array(X), np.array(y)

    @staticmethod
    def impute_week(df):
        seq_len_max = df.groupby(['family_desc', 'group_id', 'sub_geo']).apply(lambda x: x.shape[0]).max()
        data_list = []
        for idx, tmp_df in df.groupby(['family_desc', 'group_id', 'sub_geo']):
            tmp = pd.to_datetime(tmp_df['date'].unique())
            date_idx = pd.date_range(min(tmp), periods=seq_len_max, freq='W-MON')
            target_df = pd.DataFrame({
                'date': date_idx.strftime('%Y-%m-%d'),
                'in_ss_eol': 0,
                'week_index': range(len(date_idx)),
                'family_desc': idx[0],
                'group_id': idx[1],
                'sub_geo': idx[2],
                'business_type': tmp_df['business_type'].unique()[0],
                'category': tmp_df['category'].unique()[0]
            })
            target_df.loc[target_df.date <= max(tmp_df), 'in_ss_eol'] = 1
            target_df = target_df.merge(tmp_df, how='left',
                                        on=['family_desc', 'sub_geo', 'group_id', 'date', 'business_type',
                                            'category'])
            data_list.append(target_df)
        df = pd.concat(data_list, axis=0).replace(np.nan, 0)
        return df

    def run(self):
        train_data, test_data = self.train_test_split(self.stratify_vars)
        train_batch_generator = self.generate_batch(train_data, idx_col=self.stratify_vars)
        test_batch_generator = self.generate_batch(test_data, idx_col=self.stratify_vars)
        return train_batch_generator, test_batch_generator


if __name__ == "__main__":
    cols = ['sub_geo', 'group_id', 'category', 'in_ss_eol', 'week_index']
    target = ['quantity']
    cat_vars = ['sub_geo', 'group_id', 'category', 'business_type']

    df = Preprocess.read_csv('data/clean_order_v1.csv')
    df = df.loc[df.sub_geo == 'NA', :]
    df = df[['family_desc', 'group_id', 'sub_geo', 'business_type',
             'category', 'quantity', 'date']]
    df.date = pd.to_datetime(df.date).dt.strftime('%Y-%m-%d')
    df = Preprocess.impute_week(df)
    # df.to_csv("data/clean_order_imputed_v1.csv", index=False)
    dg = Preprocess(data=df, features=cols, target=target,
                    cat_vars=cat_vars, split_on=['week_index'])
    train, test = dg.run()
    print(train[0].shape, train[1].shape)
