import numpy as np
import pandas as pd
from generate_data import DataGenerator, Preprocess
from keras.models import Model, Sequential
from keras.layers import Input, LSTM, Dense
import keras.backend as K
import tensorflow as tf
from keras.losses import poisson

class seq2seq:
    def __init__(self, input_data=None, layer_hidden_size=None, loss=None):
        self.input_data = input_data
        self.layers_size = layer_hidden_size if layer_hidden_size is not None else [512, 256]
        self.loss = 'mse' if loss is None else loss
        self.feature_dim = None

    def build_model(self):
        model = Sequential()
        model.add(LSTM(self.layers_size[0], input_shape=(None, self.feature_dim), return_sequences=True))
        for i in self.layers_size[1:]:
            model.add(LSTM(i, return_sequences=True))
        model.add(Dense(1, activation=K.exp))

        assert (self.loss in ('mse', 'poisson')), "loss function not supported"
        if self.loss == 'mse':
            model.compile(optimizer='rmsprop', loss='mse')
        elif self.loss == 'poisson':
            model.compile(optimizer='rmsprop', loss=poisson)
        print(model.summary())
        return model

    def fit(self):
        print("begin fitting...")
        train, test = self.input_data.run()
        self.feature_dim = train[0].shape[-1]
        model = self.build_model()
        model.fit(train[0], train[1], epochs=20, batch_size=16, shuffle=True,
                  validation_data=test)

    def predict(self, model, data):
        output = model.predict(data)
        return output


if __name__ == '__main__':
    # random generating
    batch_size = 128
    lstm_size = [512, 512]
    num_rows = {'train': 1000, 'valid': 50, 'test': 50}
    seq_len = 10

    # data = DataGenerator(num_rows=num_rows,
    #                      feature_num=feature_num,
    #                      batch_size=batch_size,
    #                      seq_len=seq_len).generate_data()

    # order data
    cols = ['sub_geo', 'group_id', 'category', 'in_ss_eol', 'week_index'] + \
            ['config' + str(i) for i in range(10)]
    target = ['quantity']
    cat_vars = ['sub_geo', 'group_id', 'category', 'business_type']

    # df = Preprocess.read_csv('data/clean_order_v1.csv')
    # df = df[['family_desc', 'group_id', 'sub_geo', 'business_type', 'category', 'quantity', 'date']]
    # df.date = pd.to_datetime(df.date).dt.strftime('%Y-%m-%d')
    # df = Preprocess.impute_week(df)
    # df.to_csv("data/clean_order_imputed_v1.csv", index=False)

    # order data
    df = Preprocess.read_csv('data/clean_order_imputed_v1.csv')
    df = df[['family_desc', 'group_id', 'sub_geo', 'business_type', 'category', 'quantity',
             'date', 'in_ss_eol', 'week_index']]
    df.date = pd.to_datetime(df.date).dt.strftime('%Y-%m-%d')

    # config data
    config_df = Preprocess.read_csv('data/family_config_tbg.csv')

    # merge
    df = df.merge(config_df, how='left', on=['family_desc', 'sub_geo', 'group_id']) \
             .replace(np.nan, -9999)

    # generator
    dg = Preprocess(data=df, features=cols, target=target,
                    cat_vars=cat_vars, split_on=['week_index'])

    # model
    s2s = seq2seq(layer_hidden_size=lstm_size,
                  input_data=dg,
                  loss='poisson')
    s2s.fit()

